// ver 2 - переделал на массив, чтобы можно было удаляь последний при помощи Backspace

let keyButtons = document.querySelectorAll('.btn'),
    keyBlock = document.querySelector('.keyboard-result'),
    keysArr = [],
    activeButton

if(keyButtons)
{
    keyButtons = [...keyButtons]
}

document.body.addEventListener('keydown', (event) => {
    // console.log(event)

    activeButton = document.querySelector('.btn-active')
    if(activeButton)
    {
        activeButton.classList.remove('btn-active')
    }

    if(event.key === 'Backspace')
    {
        if(keysArr.length > 0)
        {
            keysArr.length = keysArr.length - 1
        }
    }

    keyButtons.map(btn => {
        // if(event.code === `Key${btn.innerText}` || event.code === btn.innerText)
        if(event.key === btn.innerText.toLowerCase() || event.code === btn.innerText)
        {
            btn.classList.add('btn-active')
            keysArr.push(event.key)
        }
        
    })
    keyBlock.innerText = keysArr.join (' + ')
})